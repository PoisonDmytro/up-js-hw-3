//Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна
console.log("Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна");
console.log("дозволяє зручно витягувати конкретні значення зі структурних об'єктів та масивів та використовувати їх безпосередньо, замість того, щоб використовувати повні шляхи до властивостей або елементів, ");

//---------------task1------------------------
console.log("---------------task1------------------------");
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

function combineClients(ar1, ar2) {
    return Array.from(new Set([...ar1, ...ar2]));
}

const combinedClients = combineClients(clients1, clients2);

console.log("База першої фірми : "+clients1);
console.log("База другої фірми : "+clients2);
console.log(combinedClients);
console.log("---------------task1------------------------");
//---------------task2------------------------
console.log("---------------task2------------------------");
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17, 
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

const charactersShortInfo = characters.map(({ name, lastName, age }) => ({ name, lastName, age }));

console.log("Новий масив данних маючий по 3 ключи в обєктах");
console.log(charactersShortInfo);
console.log("---------------task2------------------------");
//---------------task3------------------------
console.log("---------------task3------------------------");
const user1 = {
    name: "John",
    years: 30,
    // isAdmin: true
};

const { name, years, isAdmin = false } = user1;


console.log(name);      
console.log(years);     
console.log(isAdmin);   
console.log("---------------task3------------------------");
//---------------task4------------------------
console.log("---------------task4------------------------");
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422, 
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };

console.log(fullProfile);
console.log("---------------task4------------------------");
//---------------task5------------------------
console.log("---------------task5------------------------");
const books = [
    {
        name: 'Harry Potter',
        author: 'J.K. Rowling'
    }, 
    {
        name: 'Lord of the rings',
        author: 'J.R.R. Tolkien'
    }, 
    {
        name: 'The witcher',
        author: 'Andrzej Sapkowski'
    }
];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

function addBookToList(list, newBook){
    return [...list, newBook];
}

const updateBook = addBookToList(books,bookToAdd)
console.log(updateBook);
console.log("---------------task5------------------------");
//---------------task6------------------------
console.log("---------------task6------------------------");
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const newEmployee = {
    ...employee,
    age: 22,
    salary: 3000
};

console.log(newEmployee);
console.log("---------------task6------------------------");
//---------------task7------------------------
const array = ['value', () => 'showValue'];

const [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'

